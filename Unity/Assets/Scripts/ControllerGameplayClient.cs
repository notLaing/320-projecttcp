using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using TMPro;
using UnityEngine;

//ATTACHED TO: Code gameobject
//Used in OnClick for connect and submit buttons, as well as Chat functions


public class ControllerGameplayClient : MonoBehaviour
{
    //only one of these; SINGLEton
    public static ControllerGameplayClient singleton;
    private TcpClient socket = new TcpClient();
    public TMP_InputField inputHost, inputPort, inputUsername;

    private Buffer buffer = Buffer.Alloc(0);

    public GameObject PanelConnection, PanelUsername, PanelGameplay;
    public ControllerGameplay game;

    public TextMeshProUGUI ChatDisplay;
    public TMP_InputField InputDisplay;

    public enum Panel
    {
        Connection,
        Username,
        Gameplay
    }

    void Awake()
    {
        //if already set, destroy gameobject
        if (singleton)
        {
            Destroy(gameObject);
        }
        else
        {
            singleton = this;
            DontDestroyOnLoad(gameObject);
            InputDisplay.interactable = false;
            InputDisplay.onEndEdit.AddListener(OnInputFieldEdit);
        }
        //go to starting state
        GoToPanel(Panel.Connection);
    }

    public void GoToPanel(Panel panel)
    {
        if (panel == Panel.Connection)
        {
            PanelConnection.SetActive(true);
            PanelGameplay.SetActive(false);
            PanelUsername.SetActive(false);
        }
        else if (panel == Panel.Username)
        {
            PanelConnection.SetActive(false);
            PanelGameplay.SetActive(false);
            PanelUsername.SetActive(true);
        }
        else if (panel == Panel.Gameplay)
        {
            PanelConnection.SetActive(false);
            PanelGameplay.SetActive(true);
            PanelUsername.SetActive(false);
        }
    }

    public void OnButtonUsername()
    {
        string user = inputUsername.text;
        Buffer packet = PacketBuilder.Join(user);
        //send packet
        SendPacketToServer(packet);
    }

    public void OnButtonConnect()
    {
        //get the host from the input field
        //get the port from the input field
        string host = inputHost.text;
        UInt16.TryParse(inputPort.text, out ushort port);

        TryToConnect(host, port);
    }

    public async void TryToConnect(string host, int port)
    {
        if (socket.Connected) return;

        try
        {
            print("Connecting...");
            await socket.ConnectAsync(host, port);
            print("Connected.");
            GoToPanel(Panel.Username);
            InputDisplay.interactable = true;

            //start receiving packets
            StartReceivingPackets();
        }
        catch (Exception e)
        {
            print("Failed to connect...");
            GoToPanel(Panel.Connection);

            //TODO: display a message to the player
        }
    }

    private async void StartReceivingPackets()
    {
        int maxPacketSize = 4096;

        while (socket.Connected)
        {
            byte[] data = new byte[maxPacketSize];

            try
            {
                int bytesRead = await socket.GetStream().ReadAsync(data, 0, maxPacketSize);
                buffer.Concat(data, bytesRead);

                //process packets
                ProcessPackets();
            }
            catch (Exception e)
            {
                print("Error in handling data: " + e);
            }
        }
    }

    private void ProcessPackets()
    {
        if (buffer.Length < 4) return;
        
        string identifier = buffer.ReadString(0, 4);
        print(identifier + " " + buffer.ReadByte(4));

        while (buffer.Length > 3)
        {
            switch (identifier)
            {
                case "JOIN":
                    if (buffer.Length < 5) return;

                    byte joinResponse = buffer.ReadByte(4);
                    if (joinResponse == 1 || joinResponse == 2 || joinResponse == 3)
                    {
                        //swap to gameplay scene
                        GoToPanel(Panel.Gameplay);
                    }
                    else if (joinResponse == 9) //full
                    {
                        print("Game is full");
                        //send back to the first screen
                        GoToPanel(Panel.Connection);
                    }
                    else //username is bad
                    {
                        print("Username was bad, code: " + joinResponse);
                        //go back to username screen
                        GoToPanel(Panel.Username);
                    }

                    //consume data
                    buffer.Consume(5);
                    break;
                case "UPDT":
                    if (buffer.Length < 70) return;

                    byte whoseTurn = buffer.ReadByte(4);
                    byte gameStatus = buffer.ReadByte(5);
                    byte[] spaces = new byte[64];
                    for (int i = 0; i < spaces.Length; ++i)
                    {
                        spaces[i] = buffer.ReadByte(6 + i);
                    }

                    print("Game updated");
                    //TODO: switch to gameplay screen
                    GoToPanel(Panel.Gameplay);

                    //TODO: update game
                    game.UpdateFromServer(gameStatus, whoseTurn, spaces);

                    buffer.Consume(70);
                    break;
                case "CHAT":
                    if (buffer.Length < 7) return;

                    byte usernameLength = buffer.ReadByte(4);
                    ushort messageLength = buffer.ReadUInt16BE(5);

                    int fullPacketLength = 7 + usernameLength + messageLength;
                    if (buffer.Length < fullPacketLength) return;
                    string username = buffer.ReadString(7, usernameLength);
                    string message = buffer.ReadString(7 + usernameLength, messageLength);
                    Debug.Log("User length: " + usernameLength + "\nMessage length: " + messageLength + "\nUsername: " + username + "\nMessage: " + message);

                    //switch to gameplay
                    GoToPanel(Panel.Gameplay);

                    //update chat view
                    AddMessageToChatDisplay("<color=#FFFFFF>" + username + ": " + message + "</color>");

                    buffer.Consume(fullPacketLength);
                    break;
                case "LIST":
                    if (buffer.Length < 9) return;//9 is minimum: 4 (LIST) + 1 (self in server) + 1 (name length variable) + 3 (minimum name length)

                    string list = "<color=#55FF55>LIST:";
                    int numNames = buffer.ReadByte(4);
                    int listPacketLength = 5;
                    int nextLengthLocation = 5;
                    int nameLength = 0;
                    for (int i = 0; i < numNames; ++i)
                    {
                        nameLength = buffer.ReadByte(nextLengthLocation);
                        nextLengthLocation += nameLength + 1;

                        //0 1   2   3   4   5   6   7   8   9   10  11  12  13  14  15  16  17
                        //L I   S   T   3   3   T   o   m   4   L   i   l   y   3   A   r   a
                        list += "\t" + buffer.ReadString(nextLengthLocation - nameLength, nameLength);
                        listPacketLength += 1 + nameLength;
                    }
                    AddMessageToChatDisplay(list + "</color>");

                    buffer.Consume(listPacketLength);
                    break;
                default:
                    print("Unknown packet identifier...");
                    buffer.Clear();
                    break;
            }
        }//while
    }

    public async void SendPacketToServer(Buffer packet)
    {
        if (!socket.Connected) return; //not connected to server

        await socket.GetStream().WriteAsync(packet.Bytes, 0, packet.Bytes.Length);
    }

    public void SendPlayPacket(int loc)//(int x, int y)
    {
        SendPacketToServer(PacketBuilder.Play(loc));
        //SendPacketToServer(PacketBuilder.Play(x, y));
    }

    ////////////////////////////////////////////////////////////////////////chat functions below
    public void AddMessageToChatDisplay(string text)
    {
        ChatDisplay.text += text + "\n";
    }

    public void OnInputFieldEdit(string s)
    {
        SendMessagePacket(s);
        InputDisplay.text = "";
        InputDisplay.Select();
        InputDisplay.ActivateInputField();
    }

    public void SendMessagePacket(string message)
    {
        //using League of Legends command style: /cmd
        if (message[0] == '/')
        {
            message = message.Substring(1, message.Length - 1);
            string[] cmd = message.Split(' ');

            switch (cmd[0].ToLower())
            {
                case "name":
                    SendPacketToServer(PacketBuilder.Name(message));
                    break;
                case "list":
                    SendPacketToServer(PacketBuilder.List());
                    break;
            }
        }
        else
        {
            //normal chat packet
            SendPacketToServer(PacketBuilder.Chat(message));
        }
    }
}
