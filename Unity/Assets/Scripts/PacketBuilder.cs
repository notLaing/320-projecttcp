using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PacketBuilder
{
    //join
    public static Buffer Join(string username)
    {
        int packetLength = 5 + username.Length;
        Buffer packet = Buffer.Alloc(packetLength);

        packet.WriteString("JOIN");
        packet.WriteByte((byte)username.Length, 4);
        packet.WriteString(username, 5);
        return packet;
    }

    //chat
    public static Buffer Chat(string message)
    {
        int packetLength = 5 + message.Length;
        Buffer packet = Buffer.Alloc(packetLength);

        packet.WriteString("CHAT");
        packet.WriteByte((byte)message.Length, 4);
        packet.WriteString(message, 5);
        return packet;
    }

    //play
    public static Buffer Play(int loc)//(int x, int y)
    {
        Buffer packet = Buffer.Alloc(5);//6

        //can probably compress here by assigning numbers to a grid, since 1 byte = 8 bits = 256
        packet.WriteString("PLAY");
        packet.WriteByte((byte)loc, 4);
        //packet.WriteByte((byte)x, 4);
        //packet.WriteByte((byte)y, 5);
        return packet;
    }

    //list
    public static Buffer List()
    {
        Buffer packet = Buffer.Alloc(4);

        packet.WriteString("LIST");
        return packet;
    }

    //name
    public static Buffer Name(string message)
    {
        int packetLength = 5 + message.Length;
        Buffer packet = Buffer.Alloc(packetLength);

        packet.WriteString("NAME");
        packet.WriteByte((byte)message.Length, 4);
        packet.WriteString(message, 5);
        return packet;
    }
}
