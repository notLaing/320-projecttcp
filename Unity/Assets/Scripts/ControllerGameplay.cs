using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//ATTACHED TO: Code gameobject

public enum Player
{
    Nobody,
    PlayerX,
    PlayerO
}
public class ControllerGameplay : MonoBehaviour
{
    private int columns = 8;
    private int rows = 8;
    public Tile prefab;
    private int[,] boardData;
    private Tile[,] boardUI;
    public Transform panelGameBoard;
    public Sprite ina, gura, takodachi, bloop, blank;

    // Start is called before the first frame update
    void Start()
    {
        BuildBoardUI();

        /*var myBuffer = Buffer.Alloc(1);
        var myInt = myBuffer.ReadUInt8(4);*/
    }

    private void BuildBoardUI()//undo to here
    {
        boardUI = new Tile[columns, rows];

        for (int r = 0; r < rows; ++r)
        {
            for (int c = 0; c < columns; ++c)
            {
                Tile bttn = Instantiate(prefab, panelGameBoard);
                //change the background color of the tile for the checkerboard effect
                if ((c + r) % 2 == 0)
                {
                    bttn.gameObject.GetComponentInChildren<Image>().color = new Color(1f, 1f, 1f);
                }

                //designate what image is on the tile at the start
                switch(r)
                {
                    case 0:
                    case 2:
                        if(c % 2 == 0) bttn.gameObject.GetComponentsInChildren<Image>()[1].sprite = takodachi;
                        break;
                    case 1:
                        if(c % 2 == 1) bttn.gameObject.GetComponentsInChildren<Image>()[1].sprite = takodachi;
                        break;
                    case 6:
                        if (c % 2 == 0) bttn.gameObject.GetComponentsInChildren<Image>()[1].sprite = bloop;
                        break;
                    case 5:
                    case 7:
                        if (c % 2 == 1) bttn.gameObject.GetComponentsInChildren<Image>()[1].sprite = bloop;
                        break;
                    default:
                        break;
                }

                //init the button
                bttn.Init(new GridPos(r, c), () => { ButtonClicked(bttn); });
                boardUI[r, c] = bttn;
            }
        }
    }

    private void ButtonClicked(Tile bttn)
    {
        int location = (bttn.pos.Y * 8) + bttn.pos.X;
        ControllerGameplayClient.singleton.SendPlayPacket(location);
        //ControllerGameplayClient.singleton.SendPlayPacket(bttn.pos.X, bttn.pos.Y);
    }

    public void UpdateFromServer(byte gameStatus, byte whoseTurn, byte[] spaces)
    {
        print("Update from server...");
        //all of row 1, then all of row 2, etc.
        for (int i = 0; i < spaces.Length; ++i)
        {
            byte b = spaces[i];

            int col = i % 8;//"x"
            int row = i / 8;//"y"

            boardUI[row, col].SetOwner(b);
            //print(b);
        }
    }
}
