using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;

//ATTACHED TO: Tile prefab

public struct GridPos
{
    public int X;//row
    public int Y;//col

    public GridPos(int X, int Y)
    {
        this.X = X;//row
        this.Y = Y;//col
    }

    public override string ToString()
    {
        return $"Grid pos: ({X}, {Y}) = (row, col)";
    }
}

public class Tile : MonoBehaviour
{
    public GridPos pos;
    public Button button;
    //public TextMeshProUGUI textField;
    public GameObject img;
    public Sprite ina, gura, tako, bloop, empty;

    public void Init(GridPos pos, UnityAction callback)
    {
        this.pos = pos;
        button.onClick.AddListener(callback);
    }

    public void SetOwner(byte b)
    {
        /*if (b == 0) textField.text = "";
        if (b == 1) textField.text = "X";
        if (b == 2) textField.text = "O";*/

        switch(b)
        {
            case 1:
                img.GetComponent<Image>().sprite = tako;
                break;
            case 2:
                img.GetComponent<Image>().sprite = bloop;
                break;
            case 3:
                img.GetComponent<Image>().sprite = ina;
                break;
            case 4:
                img.GetComponent<Image>().sprite = gura;
                break;
            default:
                img.GetComponent<Image>().sprite = empty;
                break;
        }
    }
}
