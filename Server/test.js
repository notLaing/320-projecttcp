const net = require("net");
const socket = net.connect({port: 8080, ip: "127.0.0.1"}, () => {
    console.log("Connected to the server.");
    
    //buff = packet
    const buff = Buffer.alloc(8);
    buff.write("JOIN");
    buff.writeUInt8(3, 4);
    buff.write("Tom", 5);
    socket.write(buff);
});

socket.on("error", e => {
    console.log(e);
});