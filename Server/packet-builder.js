exports.PacketBuilder = {
    join(responseID)
    {
        const packet = Buffer.alloc(5);

        packet.write("JOIN", 0);
        packet.writeUInt8(responseID, 4);
        return packet;
    },

    chat(userLength, user, msgLength, msg)
    {
        const packet = Buffer.alloc(7 + userLength + msgLength);

        packet.write("CHAT");
        packet.writeUInt8(userLength, 4);
        packet.writeUInt16BE(msgLength, 5);
        packet.write(user, 7);
        packet.write(msg, 7 + userLength);
        return packet;

        //0   1   2   3   4   5   6   7   8   9   10   11
        //C   H   A   T   3   0   7   T   o   m   Message
    },

    update(game)
    {
        const packet = Buffer.alloc(70);
        packet.write("UPDT", 0);
        packet.writeUInt8(game.whoseTurn, 4);
        packet.writeUInt8(game.whoHasWon, 5);

        let offset = 6;
        for(let r = 0; r < game.board.length; ++r)
        {
            for(let c = 0; c < game.board[r].length; ++c)
            {
                packet.writeUInt8(game.board[r][c], offset);
                ++offset;
            }
        }

        return packet;
    },

    list(allClients)
    {
        const arrayOfNames = [];
        const nameLengths = [];

        allClients.forEach(c => {
            if(c.username)
            {
                arrayOfNames.push(c.username);
                nameLengths.push(c.username.length);
            }
            /*else
            {
                arrayOfNames.push(c.socket.localAddress);
                nameLengths.push(c.socket.localAddress.ToString().length);
            }*/
        });

        //sum of nameLengths + length of nameLengths + 4
        const sum = nameLengths.reduce((a, b) => a + b);
        const packet = Buffer.alloc(5 + nameLengths.length + sum);

        let i = 0;
        packet.write("LIST");
        packet.writeUInt8(nameLengths.length, 4);
        while(arrayOfNames.length > 0)
        {
            packet.writeUInt8(nameLengths[0], 5 + i);
            packet.write(arrayOfNames[0], 5 + i + 1);

            i += 1 + nameLengths[0];
            arrayOfNames.splice(0, 1);
            nameLengths.splice(0,1);
        }
        return packet;

        //0   1   2   3   4   5   6   7   8   9   10   11
        //C   H   A   T   3   0   7   T   o   m   Message
    },
}