const server = require("./server").Server;
const PacketBuilder = require("./packet-builder.js").PacketBuilder;

const Game = {
    whoseTurn: 1,
    whoHasWon: 0,
    board: [
        /*[0, 0, 0],
        [0, 0, 0],
        [0, 0, 0]
        */
        [1, 0, 1, 0, 1, 0, 1, 0],
        [0, 1, 0, 1, 0, 1, 0, 1],
        [1, 0, 1, 0, 1, 0, 1, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 2, 0, 2, 0, 2, 0, 2],
        [2, 0, 2, 0, 2, 0, 2, 0],
        [0, 2, 0, 2, 0, 2, 0, 2]
    ],
    clientI: null, //player 1, Ina
    clientG: null, //player 2, Gura
    selected: null, //selected piece by current player
    destination: null, //destination tile by current player
    playMove(client, loc)//r, c)
    {
        //ignore moves after game has ended
        if(this.whoHasWon > 0) return;

        //ignore everyone except current player
        if(this.whoseTurn == 1 && client != this.clientI) return;
        if(this.whoseTurn == 2 && client != this.clientG) return;

        //ignore invalid moves
        const r = Math.floor(loc / 8);//y
        const c = loc % 8;//x
        //if(r < 0 || c < 0 || c >= this.board.length || r >= this.board[c].length) return;
        if(r < 0 || c < 0 || r >= this.board.length || c >= this.board[r].length) return;


        //server (tts) knows whose turn it is, so we decide if moves are valid here
        //two pieces: one to move, one for destination. Destination is only 1 or 2 diagonally
        let matching = false;
        let matchingKill = false;
        const matchInd = null;
        const matchKillInd = null;
        const killNum = null;
        
        if(this.whoseTurn == 1)//Ina's turn
        {
            //ignore if not an Ina piece or blank space (ignore Gura)
            if(this.board[r][c] == 2 || this.board[r][c] == 4) return;

            //set first click (selected) and reset destination on every new piece click
            if(this.board[r][c] == 1 || this.board[r][c] == 3)
            {
                this.selected = loc;
                this.destination = null;
                //await further input
                return;
            }

            //guaranteed to be 0 by this point. Ignore if no selected piece
            if(this.selected == null) return;

            //Set destination (if empty space)
            this.destination = loc;

            const testRow = Math.floor(this.selected / 8);
            const testCol = this.selected % 8;
            //see if it's a possible move. Ignore impossible moves and reset destination
            //list of possible moves (moves are already guaranteed to be in bounds)
            //+-8, then +-1, doubled for kills
            

            //                      bottom left         bottom right
            const normalMoves = [this.selected + 7, this.selected + 9];
            const killMoves = [this.selected + 14, this.selected + 18];

            if(this.board[testRow][testCol] == 1)//Takodachi (pawn)
            {
                //compare
                for(i = 0; i < 2; ++i)
                {
                    if(this.destination == normalMoves[i])
                    {
                        matching = true;
                        matchInd = i;
                    }
                    if(this.destination == killMoves[i])
                    {
                        matchingKill = true;
                        matchKillInd = i;
                    }
                }
            }
            else//Ina (king)
            {//                     top right           top left
                normalMoves.push(this.selected - 7, this.selected - 9);
                killMoves.push(this.selected - 14, this.selected - 18);
                //there are no tiles you could press to match an out-of-bounds "available" spot
                //compare
                for(i = 0; i < 4; ++i)
                {
                    if(this.destination == normalMoves[i])
                    {
                        matching = true;
                        matchInd = i;
                    }
                    if(this.destination == killMoves[i])
                    {
                        matchingKill = true;
                        matchKillInd = i;
                    }
                }
            }

            if(matching || matchingKill)
            {
                //move the piece if it isn't an edge piece going off edge (rebounding)
                switch(matchInd)
                {
                    //0 and 1: moving bottom left/right, respectively
                    //normal pieces are always able to move down to a blank spot
                    //don't need to check upperbounds since that would mean destination < 0 or
                    //destination > 63. Would not have found match
                    case 0:
                        if(testCol < 1) this.resetDestination();
                        break;
                    case 1:
                        if(testCol > 6) this.resetDestination();
                        break;

                    //2 and 3: moving top right/left, respectively. Only Ina pieces can do this
                    case 2:
                        if(testCol > 6) this.resetDestination();
                        break;
                    case 3:
                        if(testCol < 1) this.resetDestination();
                        break;
                    default:
                        break;
                }

                switch(matchKillInd)
                {
                    //we know that it is a valid spot, but we need to check if it's a spot
                    //we get to by killing a piece. Still need to check left/right bounds

                    //killing bottom left/right, respectively
                    case 0:
                        if(testCol < 2) this.resetDestination();
                        if(!(board[testRow + 1][testCol - 1] == 2 || board[testRow + 1][testCol - 1] == 4)) this.resetDestination();
                        killNum = this.selected + 7;
                        break;
                    case 1:
                        if(testCol > 5) this.resetDestination();
                        if(!(board[testRow + 1][testCol + 1] == 2 || board[testRow + 1][testCol + 1] == 4)) this.resetDestination();
                        killNum = this.selected + 9;
                        break;

                    //killing top right/left, respectively
                    case 2:
                        if(testCol > 5) this.resetDestination();
                        if(!(board[testRow - 1][testCol + 1] == 2 || board[testRow - 1][testCol + 1] == 4)) this.resetDestination();
                        killNum = this.selected - 7;
                        break;
                    case 3:
                        if(testCol < 2) this.resetDestination();
                        if(!(board[testRow - 1][testCol - 1] == 2 || board[testRow - 1][testCol - 1] == 4)) this.resetDestination();
                        killNum = this.selected - 9;
                        break;
                    default:
                        break;
                }
            }//if(matching || matchingKill)
            else
            {
                this.resetDestination();
            }
            //at this point, we know it is a valid move or kill
        }//if Ina's turn
        else//Gura's turn
        {
            //
        }

        
        const typeOfPiece = this.board[Math.floor(this.selected / 8)][this.selected % 8];
        this.board[Math.floor(this.selected / 8)][this.selected % 8] = 0;
        this.board[Math.floor(this.destination / 8)][this.destination % 8] = typeOfPiece;
        console.log("Not updated, but by now there should be a move");
        console.log("Old space should be blank: " + this.board[Math.floor(this.selected / 8)][this.selected % 8]);
        console.log("New spot should have a takodachi: " + this.board[Math.floor(this.destination / 8)][this.destination % 8]);

        //change the piece if it's a Takodachi or Bloop (pawn)
        if(typeOfPiece == 1)
        {
            //
        }
        else if(typeOfPiece == 2)
        {
            //
        }
        else if(typeOfPiece == 3)
        {
            //
        }
        else if(typeOfPiece == 4)
        {
            //
        }

        //if killed, make old spot 1 and don't toggle turn (toggle twice) if there are kills to be made with that piece
        if(matchingKill)
        {
            this.board[Math.floor(killNum / 8)][killNum % 8] = 0;
            //check for other possible kills
            this.checkKills(this.destination);
            //this.whoseTurn = (this.whoseTurn == 1) ? 2 : 1;//toggles turn
        }

        //this.whoseTurn = (this.whoseTurn == 1) ? 2 : 1;//toggles turn
        this.checkStateAndUpdate();
    },
    checkStateAndUpdate()
    {
        //TODO: check for game over
        
        const packet = PacketBuilder.update(this);
        server.broadcastPacket(packet);
    },
    resetDestination()
    {
        //reset destination, but keep selected
        this.destination = null;
        return;
    },
    checkKills(currentPiece)
    {
        //check if the given token can still kill
        switch(currentPiece)
        {
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
            case 4:
                break;
        }
    }
}

server.start(Game);