const Client = require("./client.js").Client;
const {PacketBuilder} = require("./packet-builder");

exports.Server = {
    port: 8080,
    clients: [],
    maxConnectedUsers: 8,
    start(game)
    {
        this.game = game;
        this.socket = require("net").createServer({}, c => this.onClientConnect(c));
        this.socket.on("error", e => this.onError(e));
        this.socket.listen({port: this.port}, () => this.onStartListen());
    },
    onClientConnect(socket)
    {
        console.log("New connection from " + socket.localAddress);

        if(this.isServerFull())
        {
            const packet = PacketBuilder.join(9);
            socket.end(packet);
        }
        else
        {
            const client = new Client(socket, this);
            this.clients.push(client);
        }
    },
    onError(e)
    {
        console.log("Error with listener: " + e);
    },
    onClientDisconnect(client)
    {
        //free up seats
        if(this.game.clientI == client) this.game.clientI = null;
        if(this.game.clientG == client) this.game.clientG = null;

        const index = this.clients.indexOf(client);
        this.clients.splice(index, 1);
    },
    onStartListen()
    {
        console.log("Server is now listening on port " + this.port);
    },
    isServerFull()
    {
        return(this.clients.length >= this.maxConnectedUsers)
    },
    generateResponseID(desiredUsername, client)
    {
        /*1: player "X"
        2: player "O"
        3: spectator

        (denied)
        4: username too short
        5: username too long
        6: username has invalid characters
        7: username taken
        8: username not allowed (profanity)
        9: game full; can't join*/
        if(desiredUsername.length < 3) return 4;
        if(desiredUsername.length > 12) return 5;
        
        const regex1 = /^[a-zA-Z0-9\[\]]+$/;//literal regex in js
        if(!regex1.test(desiredUsername)) return 6;//invalid characters

        let isUsernameTaken = false;
        this.clients.forEach(c => {
            if(c == client) return;
            if(c.username == desiredUsername) isUsernameTaken = true;
        });

        if(isUsernameTaken) return 7;

        const regex2 = /(fuck|fvck|shit|piss|damn)/i;
        if(regex2.test(desiredUsername)) return 8;//bad words

        //set username since it passed all the checks
        client.username = desiredUsername;

        if(this.game.clientI == client) return 1;//you are already client X
        if(this.game.clientG == client) return 2;//you are already client O
        if(this.game.clientI && this.game.clientG) return 3; //you are a spectator
        if(!this.game.clientI)
        {
            this.game.clientI = client;
            return 1;//you are now clientI
        }
        if(!this.game.clientG)
        {
            this.game.clientG = client;
            return 2;//you are clientG
        }

        return 3;//spectator
    },
    broadcastPacket(packet)
    {
        this.clients.forEach(c => {
            c.sendPacket(packet);
        })
    }
};