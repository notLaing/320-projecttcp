//include one of the following two ways
const { debug } = require("console");
const {PacketBuilder} = require("./packet-builder");
//const PacketBuilder = require("./packet-builder.js").PacketBuilder;

//represents connection, not actual client. This receives messages, not sends them
exports.Client = class Client {
    constructor(socket, server)
    {
        this.socket = socket;
        this.server = server;
        this.username = "";
        this.buffer = Buffer.alloc(0);;
        this.socket.on("error", (e) => this.onError(e));

        this.socket.on("close", () => this.onClose());

        this.socket.on("data", (d) => this.onData(d));
    }

    onError(errMessage)
    {
        console.log("Error with client: " + errMessage);
    }

    onClose()
    {
        this.server.onClientDisconnect(this);
    }

    onData(data)
    {
        this.buffer = Buffer.concat([this.buffer, data]);
        if(this.buffer.length < 4) return;
        
        const packetIdentifier = this.buffer.slice(0, 4).toString();

        switch(packetIdentifier)
        {
            case "JOIN":
                //make sure packet/data is long enough
                if(this.buffer.length < 5) return;//not enough data

                //get length of username
                const lengthOfUsername = this.buffer.readUInt8(4);
                if(this.buffer.length < 5 + lengthOfUsername) return;
                const desiredName = this.buffer.slice(5, 5 + lengthOfUsername).toString();
                console.log("Client wants name to be " + desiredName);

                //check username
                let responseType = this.server.generateResponseID(desiredName, this);

                //consume data
                this.buffer = this.buffer.slice(5 + lengthOfUsername);

                //respond
                const packet = PacketBuilder.join(responseType);
                this.sendPacket(packet);

                //new player can join a partially completed game
                setTimeout(function(){}, 100);
                if(responseType < 4)
                {
                    const packet2 = PacketBuilder.update(this.server.game);
                    this.sendPacket(packet2);
                }
                break;
            case "CHAT":
                //get the message
                const lengthOfMessage = this.buffer.readUInt8(4);
                if(this.buffer.length < 5 + lengthOfMessage) return;
                const inMsg = this.buffer.slice(5, 5 + lengthOfMessage).toString();

                //send the message
                const msgPacket = PacketBuilder.chat(this.username.length, this.username, lengthOfMessage, inMsg);
                //this.sendPacket(msgPacket);
                this.server.broadcastPacket(msgPacket);
                
                
                //consume data
                this.buffer = this.buffer.slice(5 + lengthOfMessage);
                break;
            case "LIST":
                const listPacket = PacketBuilder.list(this.server.clients);
                this.server.broadcastPacket(listPacket);
                //consume data
                this.buffer = this.buffer.slice(4);
                break;
            case "PLAY":
                if(this.buffer.length < 5) return;//6

                //const x = this.buffer.readUInt8(4);
                //const y = this.buffer. readUInt8(5);
                const loc = this.buffer.readUInt8(4);
                const row = Math.floor(loc / 8);//y
                const col = loc % 8;//x

                //console.log("User wants to play at: " + x + ", " + y);
                console.log("User wants to play at (row, col): " + row + ", " + col);

                this.buffer = this.buffer.slice(5);//6

                //do something with that info
                this.server.game.playMove(this, loc);//row, col);
                break;
            default:
                //don't recognize packet
                console.log("ERROR: packet identifier not recognized (" + packetIdentifier + ").");
                this.buffer = Buffer.alloc(0);//clear this.buffer so we can use it later
                break;
        }
    }

    sendPacket(packet)
    {
        this.socket.write(packet);
    }
}